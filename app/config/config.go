package config

import (
	"time"

	"github.com/kelseyhightower/envconfig"
)

var (
	// Cfg app global
	Cfg = Get()
)

// Config structure
type Config struct {
	ServerHost   string        `envconfig:"HOST" default:"0.0.0.0"`
	ServerPort   string        `envconfig:"PORT" default:"8000"`
	ReadTimeout  time.Duration `envconfig:"SERVER_READ_TIMEOUT" default:"60s"`
	WriteTimeout time.Duration `envconfig:"SERVER_WRITE_TIMEOUT" default:"60s"`
	LogLevel     string        `envconfig:"SERVER_LOG_LEVEL" default:"info"`
}

// Get config
func Get() *Config {
	cfg := &Config{}

	if err := envconfig.Process("", cfg); err != nil {
		panic(err)
	}

	return cfg
}
