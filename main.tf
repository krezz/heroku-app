# Configure the Heroku provider
provider "heroku" {
  email   = local.ws.heroku_email
  api_key = local.ws.heroku_api_key
}

resource "heroku_app" "krezz" {
  name   = "krezz"
  stack  = "container"
  region = local.ws.heroku_region
  # acm    = true # Hobby or above tier required for ACM

  config_vars = {
    INTERNAL_API_KEY = "TEST"
  }
}

resource "heroku_build" "krezz" {
  app = heroku_app.krezz.name

  source = {
    path = "."
  }
}

resource "heroku_formation" "krezz" {
  app      = heroku_app.krezz.id
  type     = "web"
  quantity = 1
  size     = "Free"
  depends_on = [
    heroku_build.krezz
  ]
}
