FROM golang:1.15-alpine as builder

ARG APP_BUILD_VERSION=""
ARG APP_BUILD_REVISION=""

ENV GOOS=linux \
    GARCH=amd64 \
    CGO_ENABLED=0 \
    GO111MODULE=on

WORKDIR /workspace

COPY ./app .

RUN apk update && \
    apk add --update --no-cache ca-certificates && \
    go mod download && \
    go mod verify && \
    go build -x -v -a -ldflags "-X main.version=${APP_BUILD_VERSION} -X main.revision=${APP_BUILD_REVISION}" -o /build/app .


FROM alpine:latest as production

COPY --from=builder /build/app /usr/local/bin

RUN addgroup -Sg 10001 appuser && \
    adduser -SD -g "" -s "/sbin/nologin" -h /appuser -G appuser -u 10001 appuser

USER appuser
ENTRYPOINT ["app"]