locals {
  env = {
    default = {
      heroku_email   = var.HEROKU_EMAIL
      heroku_api_key = var.HEROKU_API_KEY
      heroku_region  = "eu"
    }
  }
  env_vars = contains(keys(local.env), var.ENVIRONMENT) ? var.ENVIRONMENT : "default"
  ws       = merge(local.env["default"], local.env[local.env_vars])
}

variable "ENVIRONMENT" {
  default = "default"
}

variable "HEROKU_EMAIL" {
  default = ""
}

variable "HEROKU_API_KEY" {
  default = ""
}
